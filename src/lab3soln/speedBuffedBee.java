package lab3soln;

public class speedBuffedBee extends Bee {

    protected Bee originalBee;

    public speedBuffedBee(Bee bee){
        super(bee.garden,bee.location,bee.getEnergy());
        this.originalBee = bee;
        System.out.println("speed bee being created");
        this.loadImage("grey_bee.png");
        this.showOnPane();
    }
    @Override
    public Bee deBuff(){
        return originalBee;
    }

    public void step(){
        this.originalBee.step();
    }

    @Override
    /**
     * Moves the bee relative to its current position.
     * @param deltaX horizontal coordinate of step. Positive to the right
     * @param deltaY vertical coordinate of step. Positive downward
     */
    protected void moveBy(double deltaX, double deltaY) {
        moveTo(location.getX() + (deltaX  * 2), location.getY() + (deltaY * 2));
    }
}
