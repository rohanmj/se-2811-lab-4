package lab3soln;

import java.awt.*;

/**
 * A Bee which picks random flower, flies to it, then picks another
 */
public class RandomPathBee extends Bee {
    protected Flower target = null;

    /**
     * @param garden Garden in which the Bee lives
     * @param startingLocation The Bee's initial location. Not currently checked if out of bounds.
     * @param initialEnergy The Bee's initial energy. Will be set to 0 if greater than 0.
     */
    public RandomPathBee(Garden garden, Point startingLocation, int initialEnergy) {
        super(garden, startingLocation, initialEnergy);
    }

    /**
     * Picks a random flower and flies on the diagonal closest to it.
     * Moves by Garden.STEP_DISTANCE along each axis. (So total distance along diagonal is sqrt(2)*Garden.STEP_DISTANCE.)
     * Upon reaching the flower, it visits it picks a new flower.
     */
    @Override
    public void step() {
        if (target == null) {
            target = garden.randomFlower();
        }
        if (onTopOf(target)) {
            target.visitedBy(this);
            target = null;
        } else {
            double deltaX = Math.copySign(Garden.STEP_DISTANCE,
                                          target.getLocation().getX() - location.getX());
            double deltaY = Math.copySign(Garden.STEP_DISTANCE,
                                          target.getLocation().getY() - location.getY());
            moveBy(deltaX, deltaY);
        }
    }
}
