package lab3soln;

public class confusedBee extends Bee {

    protected Bee originalBee;
    protected Flower target = null;
    boolean confused = true;

    public confusedBee(Bee bee){
        super(bee.garden,bee.location,bee.getEnergy());
        this.originalBee = bee;
        this.buffFlag = true;
        System.out.println("confused bee being created");
        this.loadImage("confused_bee.png");
        this.showOnPane();
    }

    @Override
    public Bee deBuff(){
        return originalBee;
    }

    /**
     * Picks a random flower and flies on the diagonal closest to it.
     * Moves by Garden.STEP_DISTANCE along each axis. (So total distance along diagonal is sqrt(2)*Garden.STEP_DISTANCE.)
     * Upon reaching the flower, it visits it picks a new flower.
     */
    @Override
    public void step() {
        if (target == null && confused) {
            target = garden.randomTrap();
        }else{
            originalBee.step();
        }
        if (onTopOf(target)) {
            target.visitedBy(this);
            target = null;
        } else {
            double deltaX = Math.copySign(Garden.STEP_DISTANCE,
                    target.getLocation().getX() - location.getX());
            double deltaY = Math.copySign(Garden.STEP_DISTANCE,
                    target.getLocation().getY() - location.getY());
            moveBy(deltaX, deltaY);
        }
    }
}
