package lab3soln;

import java.awt.*;

/** ScanBee moves top to bottom and then dies once it reaches the bottom right */
public class ScanBee extends Bee {
    private boolean goingRight = true;

    /**
     * @param garden Garden in which the Bee lives
     * @param startingLocation The Bee's initial location. Not currently checked if out of bounds.
     * @param initialEnergy The Bee's initial energy. Will be set to 0 if greater than 0.
     */
    public ScanBee(Garden garden, Point startingLocation, int initialEnergy) {
        super(garden, startingLocation, initialEnergy);
    }

    /**
     * Flies in a lawnmower pattern over the garden.
     *
     * Scans back and forth in horizontal lines.
     *
     * Dies upon reaching the bottom corner!
     */
    @Override
    public void step() {
        long x = Math.round(location.getX());
        long y = Math.round(location.getY());
        if (goingRight) {
            if (x + garden.STEP_DISTANCE < garden.MAX_X)
                moveBy(garden.STEP_DISTANCE, 0);
            else {
                goingRight = false;
                moveBy(0, garden.STEP_DISTANCE);
            }
        } else {
            if (x > 0)
                moveBy(-garden.STEP_DISTANCE, 0);
            else {
                goingRight = true;
                moveBy(0, garden.STEP_DISTANCE);
            }
        }
        // if reach bottom right corner, the bee dies
        if (location.getX() + Garden.STEP_DISTANCE >= garden.MAX_X
              && location.getY() + Garden.STEP_DISTANCE >= garden.MAX_Y) {
            // the bee's work is done and it dies
            energy = 0;
        }
    }
}
