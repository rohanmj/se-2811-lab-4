package lab3soln;

public class shieldBuffedBee extends Bee {
    protected Bee originalBee;
    private boolean shieldActive = true;

    public shieldBuffedBee(Bee bee){
        super(bee.garden,bee.location,bee.getEnergy());
        this.originalBee = bee;
        this.buffFlag = true;
        System.out.println("shielded bee being created");
        this.loadImage("shield_bee.png");
        this.showOnPane();
    }
    @Override
    public Bee deBuff(){
        return originalBee;
    }

    public void step(){
        originalBee.step();
        this.location = originalBee.location;
        this.energy--;
    }

    @Override
    public void changeEnergy(int delta){
        if(shieldActive){
            this.setEnergy(energy);
            energyDisplay.setText("" + energy);
            shieldActive = false;
        } else{
            this.setEnergy(energy + delta);
            energyDisplay.setText("" + energy);
        }
    }


}
